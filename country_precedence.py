# Copyright 2021 Grasbauer
# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.model import fields
from trytond.pool import PoolMeta


__all__ = ['Country']


class Country(metaclass=PoolMeta):
    __name__ = 'country.country'

    precedence = fields.Integer('Precedence rating')

    @classmethod
    def default_precedence(cls):
        return 50

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('precedence', 'ASC'))
