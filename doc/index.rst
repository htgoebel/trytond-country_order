==========================
Country Order
==========================

-------------------------------------------------------------
Precedence rating for countries in Tryton
-------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Copyright: 2021 by Hartmut Goebel
:Licence:   GNU General Public License v3 or later (GPLv3+)
:Homepage:  http://crazy-compilers.com/tryton
:Download:  https://pypi.org/project/trytond_country_order


The *Country Order* module extends Tryton's *Country* model
by a precedence rating for each country.
This allows to get order `countries <model-country.country>`
in a precedence for your needs.

A lower precedence value means more important.
The precedence value defaults to 50.

For example:
In Germany, you might want Germany on top of the list,
as this is the country used most often in your organization.
Near the top you might want to have the other countries
where German is spoken
followed by countries around Germany and the remaining EU.

To achieve this, you might set the precedence values as follows:

=========== ===
Germany       0
Austria       5
Switzerland   5
France       10
Belgium      10
…
Italy        20
Slovenia     20
=========== ===

To move countries near the end of the list,
just set the precedence value to be higher then the default of 50:

=========== ===
Antarctica  100
=========== ===
