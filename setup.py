#!/usr/bin/env python3
# This file was originally taken from Tryton.
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import re
from configparser import ConfigParser
from setuptools import setup, find_packages


def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
        major_version, minor_version + 1)
    return require


with open(os.path.join(os.path.dirname(__file__), 'tryton.cfg')) as fh:
    config = ConfigParser()
    config.read_file(fh)
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)
name = 'trytond_country_order'


download_url = 'http://downloads.tryton.org/%s.%s/' % (
    major_version, minor_version)
if minor_version % 2:
    version = '%s.%s.dev0' % (major_version, minor_version)
    download_url = (
        'hg+http://hg.tryton.org/modules/%s#egg=%s-%s' % (
            name[8:], name, version))
local_version = []
if os.environ.get('CI_JOB_ID'):
    local_version.append(os.environ['CI_JOB_ID'])
else:
    for build in ['CI_BUILD_NUMBER', 'CI_JOB_NUMBER']:
        if os.environ.get(build):
            local_version.append(os.environ[build])
        else:
            local_version = []
            break
if local_version:
    version += '+' + '.'.join(local_version)


requires = [get_require_version('trytond_%s' % dep)
            for dep in info.get('depends', [])
            if not re.match(r'(ir|res)(\W|$)', dep)
            ]
requires.append(get_require_version('trytond'))


tests_require = [get_require_version('proteus'), 'pycountry']
dependency_links = []
if minor_version % 2:
    dependency_links.append(
        'https://trydevpi.tryton.org/?local_version='
        + '.'.join(local_version))

setup(version=version,
      download_url=download_url,
      setup_requires = ["setuptools >= 39.2.0"],
      python_requires='>=3.6',
      install_requires=requires,
      dependency_links=dependency_links,
      tests_require=tests_require,
      )
