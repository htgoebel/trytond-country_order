==============
Country Order
==============

Imports::

    >>> from proteus import Model
    >>> from trytond.tests.tools import activate_modules

Activate modules::

    >>> config = activate_modules(['country_order'])

Test default precedece::

    >>> Country = Model.get('country.country')
    >>> belgium = Country(name="Belgium", code='BE')
    >>> belgium.precedence
    50

Test changing precedece works::

    >>> belgium.precedence = 1
    >>> belgium.save()
    >>> belgium.precedence
    1

Countries with lower precedence (higher precedence value) shall be
ordered behind, while still sorted alphabetically::

    >>> for name in ("Germany", "Austria", "Aaaa", "Xxxx"):
    ...    Country(name=name, code=name[:2].upper(), precedence=10).save()
    ...
    >>> [c.name for c in Country.find()]
    ['Belgium', 'Aaaa', 'Austria', 'Germany', 'Xxxx']

Country with higher precedence (lower precedence value) shall be
ordered before, while still sorted alphabetically::

    >>> Country(name="Zzzz", code="ZZ", precedence=0).save()
    >>> [c.name for c in Country.find()]
    ['Zzzz', 'Belgium', 'Aaaa', 'Austria', 'Germany', 'Xxxx']
    >>> Country(name="Zaza", code="ZA", precedence=0).save()
    >>> [c.name for c in Country.find()]
    ['Zaza', 'Zzzz', 'Belgium', 'Aaaa', 'Austria', 'Germany', 'Xxxx']
